var fs = require('fs');
var Steam = require('steam');
var JSON = require('JSON');


var groupID = ''; //64 Bit ID of the steam chat you want the bot to sit in
var steamName = ''; //Steam bot username
var steamPass = ''; //Steam bot password
var steamRecord = ''; //Steam user who will receive bot messages
var steamAuthCode = ''; //Optional, does this bot need an Authcode to operate, E.G. is Steamguard enabled?
var botOn = 1;

//Log options for logging into the steam bottom

var logOnOptions = {
  account_name: steamName,
  password: steamPass,
  auth_code: steamAuthCode,
  sha_sentryfile: (fs.existsSync('sentryfile') ? fs.readFileSync('sentryfile') : undefined)
};

// if we've saved a server list, use it
if (fs.existsSync('servers')) {
  Steam.servers = JSON.parse(fs.readFileSync('servers'));
}

var steamClient = new Steam.SteamClient();
var steamUser = new Steam.SteamUser(steamClient);
var steamFriends = new Steam.SteamFriends(steamClient);

steamClient.connect();
steamClient.on('connected', function() {
  steamUser.logOn(logOnOptions);
});

steamClient.on('logOnResponse', function(logonResp) {
  if (logonResp.eresult == Steam.EResult.OK) {
    console.log('Logged in!');
	steamFriends.setPersonaState(Steam.EPersonaState.Online); // to display your bot's status as "Online"
	steamFriends.setPersonaName('ALKABOT'); 
	steamFriends.joinChat(groupID); // the group's SteamID as a string


  }
});

steamClient.on('servers', function(servers) {
  fs.writeFile('servers', JSON.stringify(servers));
});

steamUser.on('updateMachineAuth', function(sentry, callback) {
  fs.writeFileSync('sentry', sentry.bytes);
  callback({ sha_file: getSHA1(sentry.bytes) });
});


steamFriends.on('message', function(source, message, type, chatter) {
	
	switch (message){
	
		case '!off':
			if(source==steamRecord){ botOn = 0; steamFriends.sendMessage(steamRecord, 'Bot disabled' , Steam.EChatEntryType.ChatMsg); steamFriends.leaveChat(groupID); }
		break;
		case '!on':
			if(source==steamRecord){ botOn = 1; steamFriends.sendMessage(steamRecord, 'Bot activated' , Steam.EChatEntryType.ChatMsg); steamFriends.joinChat(groupID); }
		break;

		
	}
	
		//Two way connections
		console.log('Received message:' +  message + source);
		if(source == groupID && message.length >= 1){
			
			if(botOn>=1) {steamFriends.sendMessage(steamRecord, steamFriends.personaStates[chatter || source].player_name + ' : ' + message, Steam.EChatEntryType.ChatMsg); }
			
		}else{
			
			if(source==steamRecord && botOn>=1){ steamFriends.sendMessage(groupID, message, Steam.EChatEntryType.ChatMsg); }
			
		}

});