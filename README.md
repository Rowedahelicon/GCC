# What is this?

This is a node.js script that incorporates Node Steam (https://github.com/seishun/node-steam) for the use of making a persona chat proxy for steam group chats. Designed for those forced to use Steam Mobile.

# How do I use it?

Edit the following variables in the attached .js script...

var groupID = ''; //64 Bit ID of the steam chat you want the bot to sit in

var steamName = ''; //Steam bot username

var steamPass = ''; //Steam bot password

var steamRecord = ''; //Steam user who will receive bot messages

var steamAuthCode = ''; //Optional, does this bot need an Authcode to operate, E.G. is Steamguard enabled?

Then run it! I recommend using Forever to keep it going.

# Anything else?

!on turns it on
!off turns it off

Easy as that!